import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import CardDetails from '../../components/Card/CardDetails';
const MoviePage = ({
  match: {
    params: { id },
  },
}) => {
  const [details, setDetails] = useState({});
  const home = useSelector(state => {
    return state.home;
  });
  const filterById = () => {
    return (
      home &&
      home.movies &&
      home.movies.results &&
      home.movies.results.find(result => result.id === parseInt(id))
    );
  };
  useEffect(() => {
    setDetails(filterById());
  }, [id] );
  return (
   <CardDetails details={details}/>
  );
};

export default MoviePage;
