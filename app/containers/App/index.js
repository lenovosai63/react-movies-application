import React from 'react';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import HomePage from 'containers/HomePage/Loadable';
import Header from 'components/Header';
import GlobalStyle from '../../global-styles';
import MoviePage from '../MoviesPage';

const AppWrapper = styled.div`
  min-height: 100vh;
  color: #ffffff;
  overflow-y: hidden;
  background-image: linear-gradient(0deg, #171225 0, #21174a 100%) !important;
  * {
    box-sizing: border-box;
  }
`;

export default function App() {
  return (
    <AppWrapper>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/movie/:id" component={MoviePage} />
      </Switch>
      <GlobalStyle />
    </AppWrapper>
  );
}
