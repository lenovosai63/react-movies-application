import styles from 'styled-components';

const Wrapper = styles.div`
display:flex;
flex-wrap:wrap;
justify-content:space-around;
`;
export default Wrapper;
