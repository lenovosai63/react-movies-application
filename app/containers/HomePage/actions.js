

import { ALL_MOVIES_DATA_FAILURE, ALL_MOVIES_DATA_REQUEST, ALL_MOVIES_DATA_SUCEESS, ALL_MOVIES_FAILURE, ALL_MOVIES_REQUEST, ALL_MOVIES_SUCEESS,MOVIES_BY_PAGE_REQUEST, MOVIES_BY_PAGE_SUCCESS } from "./constants"
export const getAllMoviesRequest=()=>{
  return {
    type:ALL_MOVIES_REQUEST,
    payload:1
  }
}
export const getAllMoviesList=(data)=>{
  return{
    type:ALL_MOVIES_SUCEESS,
    payload:data
  }
}
export const moviesFailure=(error)=>{
  return{
    type:ALL_MOVIES_FAILURE,
    payload:error
  }
}
export const getAllMoviesListByPage=()=>{
  return{
    type:MOVIES_BY_PAGE_SUCCESS,
  }
}
export const  getAllMoviesListByPageRequest=(pageNo)=>{
  return{
    type:MOVIES_BY_PAGE_REQUEST,
    payload:pageNo
  }
}
export const getAllMoviesDataRequest=()=>{
  return {
    type:ALL_MOVIES_DATA_REQUEST,
    payload:1
  }
}
export const getAllMoviesDataList=(data)=>{
  return{
    type:ALL_MOVIES_DATA_SUCEESS,
    payload:data
  }
}
export const moviesDataFailure=(error)=>{
  return{
    type:ALL_MOVIES_DATA_FAILURE,
    payload:error
  }
}