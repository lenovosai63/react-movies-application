import {
  ALL_MOVIES_DATA_FAILURE,
  ALL_MOVIES_DATA_REQUEST,
  ALL_MOVIES_DATA_SUCEESS,
  ALL_MOVIES_FAILURE,
  ALL_MOVIES_REQUEST,
  ALL_MOVIES_SUCEESS,
  MOVIES_BY_PAGE_FAILURE,
  MOVIES_BY_PAGE_REQUEST,
  MOVIES_BY_PAGE_SUCCESS,
} from './constants';
import { call, put, takeLatest } from 'redux-saga/effects';

const fetchAllMovies = async url => {
  const response = await fetch(url);
  return await response.json();
};
function* getMoviesData(page) {
  console.log('GET Movie Data Was Called');
  const api_key = 'eba18dc53c335f7353a1e61d99a9a2a0';
  const url = `https://api.themoviedb.org/3/discover/movie?page=${page}&sort_by=popularity.desc&api_key=${api_key}`;
  try {
    const moviesData = yield call(fetchAllMovies, url);
    yield put({ type: ALL_MOVIES_SUCEESS, payload: moviesData });
  } catch (err) {
    yield put({ type: ALL_MOVIES_FAILURE, payload: err });
  }
}

function* getByPage(page = { payload: 1 }) {
  console.log('getPage Wa scalleed');
  const api_key = 'eba18dc53c335f7353a1e61d99a9a2a0';
  const url = `https://api.themoviedb.org/3/discover/movie?page=${
    page.payload
  }&sort_by=popularity.desc&include_adult=false&api_key=${api_key}`;
  try {
    const moviesData = yield call(fetchAllMovies, url);
    yield put({ type: MOVIES_BY_PAGE_SUCCESS, payload: moviesData });
  } catch (err) {
    yield put({ type: MOVIES_BY_PAGE_FAILURE, payload: err });
  }
}
function* getAllMoviesData() {
  const api_key = 'eba18dc53c335f7353a1e61d99a9a2a0';

  const url = `https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=${api_key}`;
  try {
    const moviesData = yield call(fetchAllMovies, url);
    yield put({ type: ALL_MOVIES_DATA_SUCEESS, payload: moviesData });
  } catch (err) {
    yield put({ type: ALL_MOVIES_DATA_FAILURE, payload: err });
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* getData() {
  yield takeLatest(ALL_MOVIES_REQUEST, getMoviesData);
  yield takeLatest(MOVIES_BY_PAGE_REQUEST, getByPage);
  yield takeLatest(ALL_MOVIES_DATA_REQUEST, getAllMoviesData);
}
