import React, { useEffect } from 'react';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { useDispatch, useSelector } from 'react-redux';
import {
  getAllMoviesListByPageRequest,
  getAllMoviesRequest,
  getAllMoviesDataRequest,
} from './actions';
import saga from './saga';
import reducer from './reducer';
import Wrapper from './styles';
import Card from '../../components/Card/Card';
import LoadingIndicator from '../../components/LoadingIndicator';
import InfiniteScroll from 'react-infinite-scroll-component';
const key = 'home';
let count = 1;
const HomePage = () => {
  const dispatch = useDispatch();
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatch(getAllMoviesRequest());
    dispatch(getAllMoviesDataRequest());
  }, [dispatch]);
  const home = useSelector(state => {
    return state.home;
  });
  const fetchData = () => {
    count = home.movies.page + 1;
    dispatch(getAllMoviesListByPageRequest(count));
  };

  return (
    <>
      <h1>All Movies</h1>
      <InfiniteScroll
        dataLength={
          home &&
          home.movies &&
          home.movies.results &&
          home.movies.results.length
        }
        next={fetchData}
        hasMore={true}
        loader={<LoadingIndicator />}
        endMessage={
          <p style={{ textAlign: 'center' }}>
            <b>Yay! You have seen it all</b>
          </p>
        }
      >
        <Wrapper>
          {home &&
            home.movies &&
            home.movies.results &&
            home.movies.results.length > 0 &&
            home.movies.results.map(result => (
              <Card key={result.id} {...result} />
            ))}
        </Wrapper>
      </InfiniteScroll>
    </>
  );
};

export default HomePage;
