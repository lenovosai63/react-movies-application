import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import {  getAllMoviesRequest } from '../actions';
import { searchData } from './actions';
const SearchBarStyles = styled.input`
  border: none;
  font-style: 15px;
  font-weight: bold;
  width: 300px;
  padding: 5px;
  &:hover {
    background-color: #cccccc;
    transition: all 3s ease-in-out;
  }
`;

const Search = () => {
  const [searchText, setSearchText] = useState('');
  const dispatch = useDispatch();
  return (
    <SearchBarStyles
      type="search"
      placeholder="Enter Movies to Search..."
      value={searchText}
      onChange={e => {
        setSearchText(e.target.value);
        if (e.target.value !== '') {
          dispatch(searchData(e.target.value));
        } else {
          dispatch(getAllMoviesRequest());
        }
      }}
    />
  );
};

export default Search;
