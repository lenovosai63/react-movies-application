
import { SEARCH_DATA } from './constants';

export const searchData = text => {
  return { type: SEARCH_DATA, payload: text };
};
