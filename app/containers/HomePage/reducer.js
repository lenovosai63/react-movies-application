import produce from 'immer';
import {
  ALL_MOVIES_DATA_FAILURE,
  ALL_MOVIES_DATA_REQUEST,
  ALL_MOVIES_DATA_SUCEESS,
  ALL_MOVIES_FAILURE,
  ALL_MOVIES_REQUEST,
  ALL_MOVIES_SUCEESS,
  MOVIES_BY_PAGE_FAILURE,
  MOVIES_BY_PAGE_REQUEST,
  MOVIES_BY_PAGE_SUCCESS,
} from './constants';
import { SEARCH_DATA } from './SearchPages/constants';

// The initial state of the App
export const initialState = {
  movies: [],
  loading: false,
  error: '',
  AllMoviesData:[],
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case ALL_MOVIES_REQUEST:
        draft.loading = true;
        break;
      case ALL_MOVIES_SUCEESS:
        draft.loading = false;
        draft.movies = action.payload;
        break;
      case ALL_MOVIES_FAILURE:
        draft.loading = false;
        draft.error = action.payload;
        break;

        case ALL_MOVIES_DATA_REQUEST:
          draft.loading = true;
          break;
        case ALL_MOVIES_DATA_SUCEESS:
          draft.loading = false;
          draft.AllMoviesData = [...state.AllMoviesData,...action.payload.results]
          break;
        case ALL_MOVIES_DATA_FAILURE:
          draft.loading = false;
          draft.error = action.payload;
          break;

      case MOVIES_BY_PAGE_REQUEST:
        draft.loading = true;
        break;
      case MOVIES_BY_PAGE_SUCCESS:
        draft.loading = false;
        draft.movies = {
          page: action.payload.page,
          results: [...state.movies.results, ...action.payload.results],
        };
        break;
      case MOVIES_BY_PAGE_FAILURE:
        draft.loading = false;
        draft.error = action.payload;
        break;
        
      case SEARCH_DATA:
        draft.movies.results = state.AllMoviesData.filter(data => {
          return data.original_title
            .toLowerCase()
            .includes(action.payload.toLowerCase());
        });
        break;
      default:
        return state;
    }
  });

export default homeReducer;
