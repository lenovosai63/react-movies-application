import styled from 'styled-components';

export default styled.div`
 display: flex;
 color:black;
 p{
   padding-left:20px;
   padding-right:30px;
 }
`;
