import React from 'react';
import { CardWrapper, ContentWrapper, Title } from './styles';
import { Link } from 'react-router-dom';
const imagePath = 'http://image.tmdb.org/t/p/w500/';

const Card = ({ poster_path, vote_average, title, vote_count, id }) => {
  return (
    <CardWrapper as={Link} to={`/movie/${id}`}>
      <img
        src={imagePath.concat(poster_path)}
        style={{ height: '300px', width: '100%', objectFit: 'cover' }}
      />
      <ContentWrapper>
        <Title>{title}</Title>
      </ContentWrapper>
    </CardWrapper>
  );
};

export default Card;
