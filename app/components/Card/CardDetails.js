import React from 'react';
import { Link } from 'react-router-dom';
import {
  CalenderIcon,
  CardDetailsWrapper,
  PopularityIcon,
  ThumbsDownIcon,
  ThumbsUpIcon,
  Button
} from './styles';

const CardDetails = ({ details }) => {
  const {
    poster_path,
    original_title,
    overview,
    popularity,
    release_date,
    vote_average,
    vote_count,
  } = details;
  const imagePath = 'http://image.tmdb.org/t/p/w500/';
  console.log(poster_path, original_title);
  return (
    <>
    <CardDetailsWrapper>
      <img
        src={imagePath.concat(poster_path)}
        style={{
          height: '300px',
          width: '100%',
          objectFit: 'contain',
          marginTop: '20px',
        }}
      />
      <h3>Title:{original_title}</h3>
      <h4 style={{ margin: '10px' }}>Description:{overview}</h4>

      <h5>
        {' '}
        <PopularityIcon />
        popularity:{popularity}
      </h5>
      <h5>
        {' '}
        <CalenderIcon /> release_date:{release_date}
      </h5>
      <h5>
        {' '}
        {vote_average > 7 ? <ThumbsUpIcon /> : <ThumbsDownIcon />}vote_average:
        {vote_average}
      </h5>
      <h5>
        {' '}
        vote_count:
        {vote_count}
      </h5>
      <Button variant="primary"  as={Link}
    to={`/`}>Go Back</Button>
    </CardDetailsWrapper>
    </>
  );
};

export default CardDetails;
